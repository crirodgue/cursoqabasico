*** Settings ***
Library     String
Library     SeleniumLibrary


*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${testUrl}      ${scheme}://${homepage}


*** KeyWords ***
Open homepage
    Open Browser    ${testUrl}  ${browser}


*** Test Cases ***
TC001: Hacer click en Contenedores
    Open homepage
    Set Global Variable     @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${contenedor}     IN      @{nombresDeContenedores}
    \   Click Element       xpath=${contenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element       xpath=//*[@id="header_logo"]/a/img
    Close Browser

TC002: Solo Abrir homepage
    Open homepage